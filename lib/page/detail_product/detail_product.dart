import 'package:bloc_cbtest/constant/styles.dart';
import 'package:bloc_cbtest/models/cocktail_response.dart';
import 'package:flutter/material.dart';

import 'package:share_files_and_screenshot_widgets/share_files_and_screenshot_widgets.dart';

// ignore: must_be_immutable
class DetailProductPage extends StatefulWidget {
  int index;
  CocktailResponse cocktailResponse;
  DetailProductPage(this.cocktailResponse, this.index);

  @override
  State<DetailProductPage> createState() => _DetailProductPageState();
}

class _DetailProductPageState extends State<DetailProductPage> {
  static GlobalKey imagekey = GlobalKey();
  int originalSize = 800;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.cocktailResponse.drinks[widget.index].strDrink),
      ),
      body: Column(
        children: [
          contentWidget(),
          InkWell(
            onTap: () async {
              ShareFilesAndScreenshotWidgets().shareScreenshot(
                  imagekey, originalSize, "Title", "Name.png", "image/png",
                  text: "ID : " +
                      widget.cocktailResponse.drinks[widget.index].idDrink);
            },
            child: Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Colors.amber, borderRadius: BorderRadius.circular(8)),
              child: Text(
                "Share to Whatsapp",
                style: Styles.description
                    .copyWith(fontSize: 11, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget contentWidget() {
    return RepaintBoundary(
      key: imagekey,
      child: Container(
        color: Colors.white,
        margin: EdgeInsets.all(16),
        padding: EdgeInsets.all(16),
        height: 160,
        child: Row(
          children: [
            Container(
                height: 100,
                width: 100,
                child: Image.network(widget
                    .cocktailResponse.drinks[widget.index].strDrinkThumb)),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Number : ${widget.index + 1}",
                      style: Styles.description.copyWith(color: Colors.black),
                    ),
                    Text(
                        "ID Product : ${widget.cocktailResponse.drinks[widget.index].idDrink}",
                        style:
                            Styles.description.copyWith(color: Colors.black)),
                    Text(
                      "Name : ${widget.cocktailResponse.drinks[widget.index].strDrink}",
                      style: Styles.description.copyWith(color: Colors.black),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  // void saveAndShare(Uint8List bytes) async {
  // final directory = await getApplicationDocumentsDirectory();
  // final image = File('${directory.path}/flutter.png');
  // image.writeAsBytesSync(bytes);

  // await Share.shareFiles(List.from(bytes));
  // }

  // void _screenshoot() {
  //   screenshotController
  //       .captureFromWidget(contentWidget())
  //       .then((capturedImage) {
  //     // Handle captured image
  //   });
  // }
}
