import 'package:bloc_cbtest/bloc/homepage/homepagenew_bloc.dart';
import 'package:bloc_cbtest/constant/pdf_api.dart';
import 'package:bloc_cbtest/constant/styles.dart';
import 'package:bloc_cbtest/page/detail_product/detail_product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late int allindex;
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomepagenewBloc()..add(HomepagenewIsStarted()),
      child: BlocListener<HomepagenewBloc, HomepagenewState>(
        listener: (context, state) {},
        child: BlocBuilder<HomepagenewBloc, HomepagenewState>(
          builder: (context, state) {
            if (state is HomepagenewIsSuccess) {
              return Scaffold(
                  appBar: AppBar(
                    backgroundColor: Colors.white,
                    elevation: 0,
                    title: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Cocktail List",
                          style: Styles.description.copyWith(
                              fontSize: 18, fontWeight: FontWeight.w600),
                        ),
                        InkWell(
                          onTap: (() async {
                            final pdfFile = await PdfApi.generateTable(
                                state.cocktailResponse, allindex);

                            PdfApi.openFile(pdfFile);
                          }),
                          child: Container(
                            child:
                                Icon(Icons.picture_as_pdf, color: Colors.black),
                          ),
                        ),
                      ],
                    ),
                  ),
                  body: SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 30),
                      height: MediaQuery.of(context).size.height,
                      child: GridView.builder(
                          shrinkWrap: true,
                          padding: const EdgeInsets.all(8),
                          addAutomaticKeepAlives: true,
                          itemCount: state.cocktailResponse.drinks.length,
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2),
                          itemBuilder: (BuildContext context, index) {
                            allindex = state.cocktailResponse.drinks.length;
                            return InkWell(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: ((context) => DetailProductPage(
                                        state.cocktailResponse, index))));
                              },
                              child: Column(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: NetworkImage(state
                                                .cocktailResponse
                                                .drinks[index]
                                                .strDrinkThumb)),
                                        borderRadius: BorderRadius.circular(8)),
                                    height: 150,
                                    width: 150,
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(top: 10),
                                    child: Text(
                                      state.cocktailResponse.drinks[index]
                                          .strDrink,
                                      style: Styles.description,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                    ),
                                  )
                                ],
                              ),
                            );
                          }),
                    ),
                  ));
            }
            return Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          },
        ),
      ),
    );
  }
}
