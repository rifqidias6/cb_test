import 'package:bloc_cbtest/models/cocktail_response.dart';
import 'package:dio/dio.dart';

class CocktailApiRepository {
  final Dio _dio = Dio();

  Future<CocktailResponse> getMundo() async {
    String url =
        'https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=Cocktail';
    Response response = await _dio.get(url);
    if (response.statusCode != 200) {
      throw Exception();
    } else {
      return CocktailResponse.fromJson(response.data);
    }
  }
}
