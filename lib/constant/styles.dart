import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Styles {
  Styles._();

  static TextStyle titleStyle = GoogleFonts.poppins(
      color: Colors.white, fontSize: 22, fontWeight: FontWeight.bold);

  static TextStyle introTitleStyle = GoogleFonts.poppins(
      color: Colors.white, fontSize: 22, fontWeight: FontWeight.bold);

  static TextStyle introDescStyle = GoogleFonts.poppins(
      color: Colors.white, fontSize: 14, fontWeight: FontWeight.w400);

  static TextStyle welcomeStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 18, fontWeight: FontWeight.w600);

  static TextStyle checkBoxLabelStyle = GoogleFonts.poppins(
      color: Colors.white, fontSize: 14, fontWeight: FontWeight.w400);

  static TextStyle buttonLabelStyle = GoogleFonts.poppins(
      color: Colors.white, fontSize: 14, fontWeight: FontWeight.w600);

  static TextStyle loginTitleStyle = GoogleFonts.poppins(
      color: Colors.white, fontSize: 24, fontWeight: FontWeight.w700);

  static TextStyle loginDescStyle = GoogleFonts.poppins(
      color: Colors.white, fontSize: 15, fontWeight: FontWeight.w400);

  static TextStyle inputStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 16, fontWeight: FontWeight.w400);

  static TextStyle pinTitleStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 18, fontWeight: FontWeight.w700);

  static TextStyle loginPinDescStyle = GoogleFonts.poppins(
      color: Colors.white, fontSize: 17, fontWeight: FontWeight.bold);

  static TextStyle whiteFontStyle = GoogleFonts.poppins(
      color: Colors.white, fontSize: 14, fontWeight: FontWeight.w400);

  static TextStyle blackFontStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 14, fontWeight: FontWeight.w400);

  static TextStyle guideTitleStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 15, fontWeight: FontWeight.w600);

  static TextStyle guideDescStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 12, fontWeight: FontWeight.w400);

  static TextStyle creditMenuTitleStyle = GoogleFonts.poppins(
    color: Colors.black,
    fontSize: 15,
    fontWeight: FontWeight.w700,
  );

  static TextStyle creditMenuSubtitleStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 12, fontWeight: FontWeight.w400);

  static TextStyle appBarTitleStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold);

  static TextStyle creditTopupTitleStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 15, fontWeight: FontWeight.bold);

  static TextStyle description = GoogleFonts.poppins(
      color: Colors.black, fontSize: 12, fontWeight: FontWeight.w400);

  static TextStyle creditDetailsLabelStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 14, fontWeight: FontWeight.w400);

  static TextStyle creditDetailsStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 14, fontWeight: FontWeight.w700);

  static TextStyle topUpDateStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 12, fontWeight: FontWeight.w400);

  static TextStyle topUpDetailsStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 13, fontWeight: FontWeight.w400);

  static TextStyle dialogTitleStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold);

  static TextStyle dialogSubtitleStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 13, fontWeight: FontWeight.w400);

  static TextStyle profileNameStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold);

  static TextStyle profileDetailStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 14, fontWeight: FontWeight.w400);

  static TextStyle profileMenuLabelStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 15, fontWeight: FontWeight.w700);

  static TextStyle outletNameStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 16, fontWeight: FontWeight.w700);

  static TextStyle receiptCodeStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 15, fontWeight: FontWeight.w400);

  static TextStyle menuItemTitleStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 13, fontWeight: FontWeight.w700);

  static TextStyle menuItemLabelStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 13, fontWeight: FontWeight.w400);

  static TextStyle menuItemQtyStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 13, fontWeight: FontWeight.w400);

  static TextStyle menuItemNoteStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 13, fontWeight: FontWeight.w400);

  static TextStyle menuItemPriceStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 13, fontWeight: FontWeight.w700);

  static TextStyle ratingLabelStyle = GoogleFonts.poppins(
      color: Colors.black, fontSize: 13, fontWeight: FontWeight.w600);

  static TextStyle badgeContentStyle = GoogleFonts.openSans(
      color: Colors.white, fontSize: 11, fontWeight: FontWeight.w600);
}
