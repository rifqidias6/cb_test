import 'dart:io';
import 'package:bloc_cbtest/models/cocktail_response.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/widgets.dart';

class PdfApi {
  static Future<File> generateTable(
      CocktailResponse cocktailResponse, index) async {
    index = index - 1;
    final pdf = Document();

    pdf.addPage(
      MultiPage(
        build: (Context context) => <Widget>[
          Wrap(
            children: List<Widget>.generate(cocktailResponse.drinks.length,
                (int index) {
              final issue = cocktailResponse.drinks[index];
              return Container(
                decoration: BoxDecoration(
                    border: Border.symmetric(vertical: BorderSide.none)),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(index.toString()),
                        Text(issue.strDrink),
                        Text(issue.idDrink),
                      ],
                    ),
                  ],
                ),
              );
            }),
          ),
        ],
      ),
    );

    // );

    return saveDocument(name: 'Credibook.pdf', pdf: pdf);
  }

  static Future<File> saveDocument({
    required String name,
    required Document pdf,
  }) async {
    final bytes = await pdf.save();

    final dir = await getApplicationDocumentsDirectory();
    final file = File('${dir.path}/$name');

    await file.writeAsBytes(bytes);

    return file;
  }

  static Future openFile(File file) async {
    final url = file.path;

    await OpenFile.open(url);
  }
}
