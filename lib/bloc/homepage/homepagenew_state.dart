part of 'homepagenew_bloc.dart';

abstract class HomepagenewState extends Equatable {
  const HomepagenewState();

  @override
  List<Object> get props => [];
}

class HomepagenewInitial extends HomepagenewState {}

class HomepagenewIsLoading extends HomepagenewState {}

class HomepagenewIsError extends HomepagenewState {}

class HomepagenewIsSuccess extends HomepagenewState {
  final CocktailResponse cocktailResponse;
  const HomepagenewIsSuccess(this.cocktailResponse);
}
