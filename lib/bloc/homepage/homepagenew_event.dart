part of 'homepagenew_bloc.dart';

abstract class HomepagenewEvent extends Equatable {
  const HomepagenewEvent();

  @override
  List<Object> get props => [];
}

class HomepagenewIsStarted extends HomepagenewEvent {
  @override
  List<Object> get props => [];
}
