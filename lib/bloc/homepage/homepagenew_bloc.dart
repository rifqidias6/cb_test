import 'package:bloc/bloc.dart';
import 'package:bloc_cbtest/models/cocktail_response.dart';
import 'package:bloc_cbtest/repositories/cocktail_repository.dart';
import 'package:equatable/equatable.dart';

part 'homepagenew_event.dart';
part 'homepagenew_state.dart';

class HomepagenewBloc extends Bloc<HomepagenewEvent, HomepagenewState> {
  HomepagenewBloc() : super(HomepagenewInitial()) {
    on<HomepagenewEvent>((event, emit) async {
      if (event is HomepagenewIsStarted) {
        emit(HomepagenewInitial());
        try {
          CocktailResponse cocktailResponse =
              await CocktailApiRepository().getMundo();
          emit(HomepagenewIsSuccess(cocktailResponse));
        } catch (e) {
          emit(HomepagenewIsError());
        }
      }
    });
  }
}
