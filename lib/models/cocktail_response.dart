// To parse this JSON data, do
//
//     final cocktailResponse = cocktailResponseFromJson(jsonString);

import 'dart:convert';

CocktailResponse cocktailResponseFromJson(String str) =>
    CocktailResponse.fromJson(json.decode(str));

String cocktailResponseToJson(CocktailResponse data) =>
    json.encode(data.toJson());

class CocktailResponse {
  CocktailResponse({
    required this.drinks,
  });

  List<Drink> drinks;

  factory CocktailResponse.fromJson(Map<String, dynamic> json) =>
      CocktailResponse(
        drinks: List<Drink>.from(json["drinks"].map((x) => Drink.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "drinks": List<dynamic>.from(drinks.map((x) => x.toJson())),
      };
}

class Drink {
  Drink({
    required this.strDrink,
    required this.strDrinkThumb,
    required this.idDrink,
  });

  String strDrink;
  String strDrinkThumb;
  String idDrink;

  factory Drink.fromJson(Map<String, dynamic> json) => Drink(
        strDrink: json["strDrink"],
        strDrinkThumb: json["strDrinkThumb"],
        idDrink: json["idDrink"],
      );

  Map<String, dynamic> toJson() => {
        "strDrink": strDrink,
        "strDrinkThumb": strDrinkThumb,
        "idDrink": idDrink,
      };

  map(List Function(dynamic e) param0) {}
}
